<?php
use \yii\web\Request;
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
return [
    'id' => 'app-frontend',
    'name'=>'Karama-Point OS',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
       'admin' => [
          'class' => 'mdm\admin\Module',
        ],
        'setup' => [
          'class' => 'app\modules\setup\Setup',
        ],
        'sales' => [
          'class' => 'app\modules\sales\Sales',
        ],
        'products' => [
          'class' => 'app\modules\product\Products',
        ],
    ],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
           'class' => 'yii\rbac\DbManager',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'baseUrl'=>$baseUrl,
            'showScriptName' => false,
            'rules' => []
        ],
        'request' => [
            'baseUrl' => $baseUrl,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
                'site/*',
                'admin/*',
            ],
        ],
    ],
    'params' => $params,
];
