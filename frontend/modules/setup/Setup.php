<?php

namespace app\modules\setup;

class Setup extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\setup\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
