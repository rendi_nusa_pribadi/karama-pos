<?php

namespace app\modules\setup\models;

use Yii;

/**
 * This is the model class for table "setup_notifications".
 *
 * @property string $id
 * @property integer $user_id
 * @property string $url
 * @property string $notification
 * @property string $status
 * @property string $created_at
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setup_notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'string'],
            [['user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['url', 'notification', 'status'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'url' => Yii::t('app', 'Url'),
            'notification' => Yii::t('app', 'Notification'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
