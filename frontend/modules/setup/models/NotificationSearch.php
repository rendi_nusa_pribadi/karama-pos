<?php

namespace app\modules\setup\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setup\models\Notification;

/**
 * NotificationSearch represents the model behind the search form about `app\modules\setup\models\Notification`.
 */
class NotificationSearch extends Notification
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'url', 'notification', 'status', 'created_at'], 'safe'],
            [['user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notification::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'notification', $this->notification])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
