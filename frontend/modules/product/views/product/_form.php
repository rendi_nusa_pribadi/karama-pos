<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\product\models\ProductType;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">
    <div class="row">
      <div class="col-md-6">
        <?php $form = ActiveForm::begin(); ?>
        
        <?= $form->field($model, 'product_type_id')->dropdownList(ProductType::find()->select(['name','id'])->indexBy('id')->column(),
        ['prompt'=>'Select Product Type']) 
        ?>
        
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'picture')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'sold_status')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>        
      </div>
    </div>

</div>
