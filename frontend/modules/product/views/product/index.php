<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\product\models\ProductType;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\product\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Product'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            [
              'attribute' => 'product_type_id', 
              'value' => 'productType.name',
              'filter' => Html::activeDropDownList($searchModel, 'product_type_id', ArrayHelper::map(ProductType::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => 'Select Product Type']),
            ],
            'sold_status:boolean',
            // 'picture',
            // 'created_at',
            // 'created_by_id',
            // 'updated_at',
            // 'updated_by_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
