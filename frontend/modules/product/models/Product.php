<?php

namespace app\modules\product\models;
use app\modules\product\models\ProductType;
use common\models\User;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property string $id
 * @property string $product_type_id
 * @property string $name
 * @property string $picture
 * @property boolean $sold_status
 * @property string $created_at
 * @property integer $created_by_id
 * @property string $updated_at
 * @property integer $updated_by_id
 *
 * @property ProductTypes $productType
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['id'], 'required'],
            [['name', 'product_type_id'], 'required'],
            [['id', 'product_type_id'], 'string'],
            [['sold_status'], 'boolean'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by_id', 'updated_by_id'], 'integer'],
            [['name', 'picture'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_type_id' => Yii::t('app', 'Product Type'),
            'name' => Yii::t('app', 'Name'),
            'picture' => Yii::t('app', 'Picture'),
            'sold_status' => Yii::t('app', 'Sold Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by_id' => Yii::t('app', 'Created By ID'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by_id' => Yii::t('app', 'Updated By ID'),
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by_id',
                'updatedByAttribute' => 'updated_by_id',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductType()
    {
        return $this->hasOne(ProductType::className(), ['id' => 'product_type_id']);
    }
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by_id']);
    }
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by_id']);
    }
}
