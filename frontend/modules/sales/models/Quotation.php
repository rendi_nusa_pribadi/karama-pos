<?php

namespace app\modules\sales\models;

use Yii;

/**
 * This is the model class for table "quotations".
 *
 * @property string $id
 * @property string $number
 * @property string $state
 * @property string $date
 */
class Quotation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quotations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['id'], 'required'],
            [['id'], 'string'],
            [['date'], 'safe'],
            [['number', 'state'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'number' => Yii::t('app', 'Number'),
            'state' => Yii::t('app', 'State'),
            'date' => Yii::t('app', 'Date'),
        ];
    }
}
