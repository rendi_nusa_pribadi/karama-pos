<?php

namespace app\modules\sales\models;

use Yii;

/**
 * This is the model class for table "breakdown_ppk_scales".
 *
 * @property string $id
 * @property string $breakdown_scale_id
 * @property string $ppk
 * @property integer $scale
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class BreakdownPpkScale extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breakdown_ppk_scales';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'breakdown_scale_id'], 'string'],
            [['scale', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['ppk'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'breakdown_scale_id' => Yii::t('app', 'Breakdown Scale ID'),
            'ppk' => Yii::t('app', 'Ppk'),
            'scale' => Yii::t('app', 'Scale'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
}
