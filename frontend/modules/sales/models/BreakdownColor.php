<?php

namespace app\modules\sales\models;
use app\modules\sales\models\Breakdown;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "breakdown_colors".
 *
 * @property string $id
 * @property string $breakdown_id
 * @property string $color_name
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $excel_file
 */
class BreakdownColor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breakdown_colors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['color_name'], 'required'],
            [['id', 'breakdown_id'], 'string'],
            ['excel_file', 'file'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['color_name'], 'string', 'max' => 255]
        ];
    }

    public function behaviors()
    {
      return [
        [
            'class' => '\yiidreamteam\upload\FileUploadBehavior',
            'attribute' => 'excel_file',
            'filePath' => '@webroot/files/[[pk]].[[extension]]',
            'fileUrl' => '/files/[[pk]].[[extension]]',
        ],
        [
            'class' => TimestampBehavior::className(),
            'createdAtAttribute' => 'created_at',
            'updatedAtAttribute' => 'updated_at',
            'value' => new Expression('NOW()'),
        ],
      ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'breakdown_id' => Yii::t('app', 'Breakdown ID'),
            'color_name' => Yii::t('app', 'Color Name'),
            'excel_file' => Yii::t('app', 'Excel File'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
    
    public function getBreakdown()
    {
        return $this->hasOne(Breakdown::className(), ['id' => 'breakdown_id']);
    }
    
    public function getBreakdownDetails()
    {
      return $this->hasMany(BreakdownDetail::className(), ['breakdown_color_id' => 'id'])
             ->orderBy('hangtag');
    }
    
    public function getBreakdownScales()
    {
      return $this->hasMany(BreakdownScale::className(), ['breakdown_color_id' => 'id']);
    }
}
