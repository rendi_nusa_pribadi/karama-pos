<?php

namespace app\modules\sales\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\sales\models\Breakdown;

/**
 * BreakdownSearch represents the model behind the search form about `app\modules\sales\models\Breakdown`.
 */
class BreakdownSearch extends Breakdown
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'style', 'body', 'drawsing', 'purchase_order_number', 'description', 'delivery_date', 'start_period_date', 'end_period_date', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Breakdown::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'delivery_date' => $this->delivery_date,
            'start_period_date' => $this->start_period_date,
            'end_period_date' => $this->end_period_date,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'style', $this->style])
            ->andFilterWhere(['like', 'body', $this->body])
            ->andFilterWhere(['like', 'drawsing', $this->drawsing])
            ->andFilterWhere(['like', 'purchase_order_number', $this->purchase_order_number])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
