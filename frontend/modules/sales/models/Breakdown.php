<?php

namespace app\modules\sales\models;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use app\modules\sales\models\BreakdownColor;
use common\models\User;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "breakdowns".
 *
 * @property string $id
 * @property string $style
 * @property string $body
 * @property string $drawsing
 * @property string $purchase_order_number
 * @property string $description
 * @property string $delivery_date
 * @property string $start_period_date
 * @property string $end_period_date
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class Breakdown extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breakdowns';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['style'], 'required'],
            [['id', 'description'], 'string'],
            [['delivery_date', 'start_period_date', 'end_period_date', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['style', 'body', 'drawsing', 'purchase_order_number'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'style' => Yii::t('app', 'Style'),
            'body' => Yii::t('app', 'Body'),
            'drawsing' => Yii::t('app', 'Drawsing'),
            'purchase_order_number' => Yii::t('app', 'Purchase Order Number'),
            'description' => Yii::t('app', 'Description'),
            'delivery_date' => Yii::t('app', 'Delivery Date'),
            'start_period_date' => Yii::t('app', 'Start Period Date'),
            'end_period_date' => Yii::t('app', 'End Period Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    
    public function getBreakdownColors()
    {
        return $this->hasMany(BreakdownColor::className(), ['breakdown_id' => 'id']);
    }
}
