<?php

namespace app\modules\sales\models;

use Yii;

/**
 * This is the model class for table "breakdown_details".
 *
 * @property string $id
 * @property string $breakdown_color_id
 * @property string $breakdown_id
 * @property string $hangtag
 * @property integer $unit_quantity
 * @property string $ppk_code
 * @property integer $ppk_quantity
 * @property double $allowance
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class BreakdownDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breakdown_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'breakdown_color_id', 'breakdown_id'], 'string'],
            [['unit_quantity', 'ppk_quantity', 'created_by', 'updated_by'], 'integer'],
            [['allowance'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['hangtag', 'ppk_code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'breakdown_color_id' => Yii::t('app', 'Breakdown Color ID'),
            'breakdown_id' => Yii::t('app', 'Breakdown ID'),
            'hangtag' => Yii::t('app', 'Hangtag'),
            'unit_quantity' => Yii::t('app', 'Unit Quantity'),
            'ppk_code' => Yii::t('app', 'Ppk Code'),
            'ppk_quantity' => Yii::t('app', 'Ppk Quantity'),
            'allowance' => Yii::t('app', 'Allowance'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
    
    public function getBreakdownColor()
    {
      return $this->hasOne(BreakdownColor::className(), ['id' => 'breakdown_color_id']);
    }
    
    public function getBreakdown()
    {
      return $this->hasOne(Breakdown::className(), ['id' => 'breakdown_id']);
    }
}
