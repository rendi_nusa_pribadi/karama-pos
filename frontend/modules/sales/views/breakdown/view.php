<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\sales\models\Breakdown */

$this->title = $model->style;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Breakdowns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="breakdown-view">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        
        <?= Html::button('Create Details', ['value' => Url::to(['breakdown/create-details', 'id'=>$model->id]), 'title' => 'Create Details', 'class' => 'showModalButton btn btn-success']); ?>
        
        <?= Html::a(Yii::t('app', 'Import Details'), ['import-details', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
        
        <?= Html::a(Yii::t('app', 'Download'), ['export', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
        
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="row">
      <div class="col-md-6">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                'style',
                'body',
                'drawsing',
                'purchase_order_number',
                'description:ntext',
                'delivery_date:date',
                'start_period_date:date',
                'end_period_date:date',
                'created_at:datetime',
                ['attribute' => 'created_by', 'value' => isset($model->createdBy) ? $model->createdBy->username : null ],
                'updated_at:datetime',
                ['attribute' => 'updated_by', 'value' => isset($model->updatedBy) ? $model->updatedBy->username : null ],
            ],
        ]) ?>
      </div>
      <div class="col-md-5">
        <table class="table table-brodered table-striped">
          <tr>
            <th>No</th>
            <th>Color</th>
            <th>Action</th>
          </tr>
          <?php foreach ($model->breakdownColors as $key => $color): ?>
            <tr>
              <td><?= $key+1; ?></td>
              <td><?= $color->color_name; ?></td>
              <td>
                <?= Html::a(Yii::t('app', 'Details'), ['/sales/breakdown-detail', 'id' => $color->id], ['class' => 'btn btn-xs btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete-detail', 'id' => $color->id], [
                    'class' => 'btn btn-xs btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </table>
      </div>
    </div>

</div>

<?php
yii\bootstrap\Modal::begin([
    'header' => '<span id="modalHeaderTitle"></span>',
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo '<div id="modalContent"></div>';
yii\bootstrap\Modal::end();
?>
