<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\sales\models\Breakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="color-form">
  <div class="row">

    <?php 
      $form = ActiveForm::begin([
        'options' => [
            'id' => 'form-add-details',
            'title' => 'Add Details'
        ],
        'enableClientValidation' => true,
        'enableAjaxValidation' => false,
      ]);
    ?>
    
    <div class="col-md-12">
      
      <?= $form->field($color, 'color_name')->textInput(['maxlength' => true]) ?>
      
      <?= $form->field($color, 'breakdown_id')->hiddenInput(['value'=>$model->id])->label(false); ?>
      
    </div>

  </div>

    <div class="form-group">
        <?= Html::submitButton($color->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $color->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
