<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\sales\models\Breakdown */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Breakdown',
]) . ' ' . $model->style;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Breakdowns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->style, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="breakdown-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
