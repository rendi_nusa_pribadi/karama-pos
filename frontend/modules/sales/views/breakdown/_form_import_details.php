<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

$this->title = Yii::t('app', 'Create Detail Breakdown');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Breakdowns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->style, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="import_details_form">
  <div class="row">
    <?php 
      $form = ActiveForm::begin([
        'options' => [
          'id' => 'form-import-details',
          'title' => 'Import Details',
          'enctype' => 'multipart/form-data',
        ],
      ]);
    ?>
    <div class="col-md-6">

      <?= $form->field($color, 'color_name')->textInput(['maxlength' => true]) ?>

      <?= $form->field($color, 'excel_file')->fileInput(); ?>

      <?= $form->field($color, 'breakdown_id')->hiddenInput(['value'=>$model->id])->label(false); ?>

    </div>
    
  </div>
    
    <div class="form-group">
        <?= Html::submitButton($color->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $color->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>
</div>