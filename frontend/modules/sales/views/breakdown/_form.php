<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\sales\models\Breakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="breakdown-form">
  <div class="row">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="col-md-4">
      
      <?= $form->field($model, 'style')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'body')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'drawsing')->textInput(['maxlength' => true]) ?>
      
    </div>
    
    <div class="col-md-4">
      
      <?= $form->field($model, 'purchase_order_number')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

      
    </div>


    <div class="col-md-4">

      <?= $form->field($model, 'delivery_date')->widget(DatePicker::classname(), ['options' => ['class' => 'form-control']]) ?>

      <?= $form->field($model, 'start_period_date')->widget(DatePicker::classname(), ['options' => ['class' => 'form-control']]) ?>

      <?= $form->field($model, 'end_period_date')->widget(DatePicker::classname(), ['options' => ['class' => 'form-control']]) ?>

    </div>

  </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
