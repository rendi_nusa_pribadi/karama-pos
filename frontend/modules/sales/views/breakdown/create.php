<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\sales\models\Breakdown */

$this->title = Yii::t('app', 'Create Breakdown');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Breakdowns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="breakdown-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
