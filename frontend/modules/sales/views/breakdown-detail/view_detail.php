<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\sales\models\BreakdownSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Breakdown Details - '.$breakdown_detail->hangtag);
$this->params['breadcrumbs'][] = ['label'=>'Breakdown', 'url' => ['/sales/breakdown/view', 'id' => $breakdown_detail->breakdown_id]];
$this->params['breadcrumbs'][] = ['label'=>'Breakdown Color ('.$breakdown_detail->breakdownColor->color_name.')', 
                        'url' => ['/sales/breakdown-detail', 'id' => $breakdown_detail->breakdown_color_id]];
$this->params['breadcrumbs'][] = $this->title;

?>

