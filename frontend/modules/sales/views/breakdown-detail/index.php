<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\sales\models\BreakdownSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Breakdown Color');
$this->params['breadcrumbs'][] = ['label'=>'Breakdown', 'url' => ['/sales/breakdown/view', 'id' => $breakdown_color->breakdown_id]];
$this->params['breadcrumbs'][] = $this->title;

?>

<h5>Color : <?= $breakdown_color->color_name; ?></h5>
<hr/>
<table class="table">
  <thead>
    <th>No</th>
    <th>Hangtag</th>
    <th>PPK Code</th>
    <th>Quantity</th>
    <th>Action</th>
  </thead>
  <tbody>
    <?php foreach ($breakdown_color->breakdownDetails as $key => $detail): ?>
      <tr>
        <td><?= $key+1 ?></td>
        <td><?= $detail->hangtag ?></td>
        <td><?= $detail->ppk_code ?></td>
        <td><?= $detail->unit_quantity ?></td>
        <td><?= Html::a(Yii::t('app', 'Details'), ['view-detail', 'id' => $detail->id], ['class' => 'btn btn-xs btn-primary']) ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<h5>Per Sizes</h5>
<hr/>
<table class="table">
  <tbody>
    <?php foreach ($breakdown_color->breakdownScales as $key => $scale): ?>
      <tr>
        <th colspan="2">Size : <?= $scale->size; ?></th>
      </tr>
      <?php foreach ($scale->ppks as $key => $ppk): ?>
        <tr>
          <td><?= $ppk->ppk; ?></td>
          <td><?= $ppk->scale; ?></td>
        </tr>
      <?php endforeach; ?>
    <?php endforeach; ?>
  </tbody>
</table>