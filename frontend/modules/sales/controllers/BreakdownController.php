<?php

namespace app\modules\sales\controllers;

use Yii;
use app\modules\sales\models\Breakdown;
use app\modules\sales\models\BreakdownColor;
use app\modules\sales\models\BreakdownDetail;
use app\modules\sales\models\BreakdownScale;
use app\modules\sales\models\BreakdownPpkScale;
use app\modules\sales\models\BreakdownSearch;

use alexgx\phpexcel;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BreakdownController implements the CRUD actions for Breakdown model.
 */
class BreakdownController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Breakdown models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BreakdownSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Breakdown model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Breakdown model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Breakdown();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Breakdown model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionImportDetails($id)
    {
      $model = $this->findModel($id);
      $color = New BreakdownColor();
      
      if ($color->load(Yii::$app->request->post()) && $color->save()) {
        $fileName = $color->getUploadedfilePath('excel_file');
        
        $sheet = \moonland\phpexcel\Excel::import($fileName, [
            'setFirstRecordAsKeys' => false, // if you want to set the keys of record column with first record, if it not set, the header with use the alphabet column on excel. 
            'setIndexSheetByName' => true, // set this if your excel data with multiple worksheet, the index of array will be set with the sheet name. If this not set, the index will use numeric. 
            // 'getOnlySheet' => 'sheet1', // you can set this property if you want to get the specified sheet from the excel data with multiple worksheet.
        ]);
        
        foreach ($sheet['details'] as $key => $details) {
          // initial for first column....
          if ($key > 1) {
            $breakdown_detail = New BreakdownDetail;
            $breakdown_detail->breakdown_color_id = $color->id;
            $breakdown_detail->breakdown_id = $color->breakdown_id;
            $breakdown_detail->hangtag = $details['A'];
            $breakdown_detail->unit_quantity = $details['B'];
            $breakdown_detail->ppk_code = $details['C'];
            $breakdown_detail->ppk_quantity = $details['D'];
            $breakdown_detail->allowance = $details['F'];
            $breakdown_detail->save(false);
          }
        }
        $ppk_code = '';
        foreach ($sheet['scales'] as $key => $scales) {

          if ($key > 1) {
            $scale = New BreakdownScale;
            $scale->breakdown_color_id = $color->id;
            $scale->size = $scales['A'];
            $scale->save(false);
          }
          
          foreach (range('C', 'Z') as $idx => $char) {
            if (isset($scales[$char]) == false) {
              break;
            }
            
            if ($key == 1) {
              $ppk_code[$char] = $scales[$char];
            }

            if ($key > 1)
            {
              $ppk_quantity = $scales[$char];
              $ppk = New BreakdownPpkScale;
              $ppk->breakdown_scale_id = $scale->id;
              $ppk->ppk = $ppk_code[$char];
              $ppk->scale = $ppk_quantity;
              $ppk->save(false);
            }
          }
        }
        Yii::$app->session->setFlash('success', "You're Brekadown Details has been imported !!");
        return $this->redirect(['view', 'id' => (string) $model->id]);
      }else {
        return $this->render('_form_import_details', [
          'model' => $model,
          'color' => $color,
        ]);
      }
    }
    
    public function actionExport($id)
    {
      $objPHPExcel = new \PHPExcel();
 
      $sheet=0;
        
      $objPHPExcel->setActiveSheetIndex($sheet);
      $foos = [
              ['firstname'=>'John',
              'lastname'=>'Doe'],
              ['firstname'=>'John',
              'lastname'=>'Jones'],
              ['firstname'=>'Jane',
              'lastname'=>'Doe'],
      ];
       
      $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
      $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);

      $objPHPExcel->getActiveSheet()->setTitle('xxx')
      ->setCellValue('A1', 'Firstname')
      ->setCellValue('B1', 'Lastname');
       
      $row = 2;
                      
      foreach ($foos as $foo) {  
              
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$foo['firstname']); 
          $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$foo['lastname']);
          $row++ ;
      }
              
      header('Content-Type: application/vnd.ms-excel');
      $filename = "breakdowns_".date("d-m-Y-His").".xlsx";
      header('Content-Disposition: attachment;filename='.$filename .' ');
      header('Cache-Control: max-age=0');
      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $objWriter->save('php://output');
    }
    
    public function actionCreateDetails($id)
    {
      $model = $this->findModel($id);
      $color = New BreakdownColor();
      
      if ($color->load(Yii::$app->request->post()) && $color->save()) {
        return $this->redirect(['view', 'id' => (string) $model->id]);
      }else if (Yii::$app->request->isAjax) {
        return $this->renderAjax('_form_details', [
          'model' => $model,
          'color' => $color,
        ]);
      }
    }
    
    public function actionDeleteDetail($id)
    {
      
      $color = BreakdownColor::findOne($id);
      $model_id = $color->breakdown_id;
      $color->delete();
      
      return $this->redirect(['view', 'id' => (string) $model_id]);
    }

    /**
     * Deletes an existing Breakdown model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Breakdown model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Breakdown the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Breakdown::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
