<?php

namespace app\modules\sales\controllers;

use Yii;
use app\modules\sales\models\BreakdownColor;
use app\modules\sales\models\BreakdownDetail;
use app\modules\sales\models\BreakdownScale;
use app\modules\sales\models\BreakdownPpkScale;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class BreakdownDetailController extends \yii\web\Controller
{
    public function actionIndex($id)
    {
        $breakdown_color = BreakdownColor::findOne($id);
        return $this->render('index', [
          'breakdown_color' => $breakdown_color,
        ]);
    }
    
    public function actionViewDetail($id)
    {
      $breakdown_detail = BreakdownDetail::findOne($id);
      return $this->render('view_detail', [
        'breakdown_detail' => $breakdown_detail,
      ]);
    }

}
