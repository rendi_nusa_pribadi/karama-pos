<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


$this->title = 'Update Profile';
$this->params['breadcrumbs'][] = $this->title;

// gender options
$genders = ['m'=>'Male', 'f'=>'Female'];
?>

<?php $form = ActiveForm::begin(['id' => 'profile-form', 
  'enableClientValidation' => false, 
  'options' => [
    'enctype' => 'multipart/form-data',
  ],
]); ?>

<div class="row">
  <div class="col-md-4">
    <?= $form->field($profile, 'first_name')->textInput() ?>

    <?= $form->field($profile, 'last_name')->textInput() ?>
    
    <?= $form->field($profile, 'gender')->dropDownList($genders, ['prompt'=>'- Select Gender -']) ?>
    
  </div>
  <div class="col-md-4">
    <?= $form->field($profile, 'employe_number')->textInput() ?>
    
    <?= $form->field($profile, 'avatar')->fileInput() ?>
    
    <?php $img = $profile->getImageFileUrl('avatar'); ?>
    
    <img src="<?= $img ?>" alt="" />
  </div>
</div>

<div class="row">
  <div class="col-md-2">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary btn-block btn-flat']) ?>
  </div>
</div>
<?php ActiveForm::end(); ?>