<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
  <div class="col-md-6">
    <?= DetailView::widget([
        'model' => $user,
        'attributes' => [
            'username',
            'email',
        ],
    ]) ?>
  </div>
  <div class="col-md-6">
    <?php if (isset($profile)): ?>
      <?= DetailView::widget([
          'model' => $profile,
          'attributes' => [
              'first_name',
              'last_name',
              [
                'attribute' => 'avatar', 
                'value' => $profile->getImageFileUrl('avatar'),
                'format' => ['image',['width'=>'120','height'=>'120']],
              ],
          ],
      ]) ?>      
    <?php endif; ?>
    
  </div>  
</div>
<?= Html::a('Update Profile', ['update-profile', 'user_id' => $_GET['user_id']], ['class'=>'btn btn-primary']) ?>