<?php

/* @var $this yii\web\View */

$this->title = 'Dashboard';
?>
<div class="site-index">
    <div class="body-content">
      <div class="row">
        <div class="col-md-3">
          <div class="info-box">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Transaction</span>
              <span class="info-box-number">93</span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->          
        </div>
      </div>
    </div>
</div>
