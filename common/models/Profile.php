<?php

namespace common\models;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "user_profiles".
 *
 * @property string $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $employe_number
 * @property string $status
 * @property string $gender
 * @property string $avatar
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['id'], 'required'],
            [['id'], 'string'],
            [['user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            ['avatar', 'file'],
            [['first_name', 'last_name', 'employe_number', 'status'], 'string', 'max' => 255],
            [['gender'], 'string', 'max' => 1]
        ];
    }
    
    public function behaviors()
    {
      return [
        [
             'class' => '\yiidreamteam\upload\ImageUploadBehavior',
             'attribute' => 'avatar',
             'filePath' => '@webroot/images/profile/[[pk]].[[extension]]',
             'fileUrl' => '@web/images/profile/[[pk]].[[extension]]',
             'thumbPath' => '@webroot/images/profile/[[profile]]_[[pk]].[[extension]]',
             'thumbUrl' => '/images/profiles/[[profile]]_[[pk]].[[extension]]',
        ],
        [
            'class' => TimestampBehavior::className(),
            'createdAtAttribute' => 'created_at',
            'updatedAtAttribute' => 'updated_at',
            'value' => new Expression('NOW()'),
        ],
      ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'employe_number' => Yii::t('app', 'Employe Number'),
            'status' => Yii::t('app', 'Status'),
            'gender' => Yii::t('app', 'Gender'),
            'avatar' => Yii::t('app', 'Avatar'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
