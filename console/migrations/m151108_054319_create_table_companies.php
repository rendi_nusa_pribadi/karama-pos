<?php

use yii\db\Schema;
use yii\db\Migration;

class m151108_054319_create_table_companies extends Migration
{
    public function up()
    {
      $this->createTable('{{%company_categories}}', [
        'id' => 'uuid default uuid_generate_v4()',
        'name' => Schema::TYPE_STRING,
      ]);
      $this->addPrimaryKey('company_categories_pk', 'company_categories', 'id');
      
      $this->createTable('{{%countries}}', [
        'id' => 'uuid default uuid_generate_v4()',
        'code' => Schema::TYPE_STRING,
        'name' => Schema::TYPE_STRING,
      ]);
      $this->addPrimaryKey('countries_pk', 'countries', 'id');
      
      $this->createTable('{{%companies}}', [
        'id' => 'uuid default uuid_generate_v4()',
        'name' => Schema::TYPE_STRING,
        'description' => Schema::TYPE_TEXT,
        'address' => Schema::TYPE_TEXT,
        
        'country_id' => 'uuid',
        'owner_id' => Schema::TYPE_INTEGER,
        'company_category_id' => 'uuid',
        'maximum_member' => Schema::TYPE_INTEGER,
        
        'created_at' => Schema::TYPE_DATETIME,
        'created_by' => Schema::TYPE_INTEGER,
        'updated_at' => Schema::TYPE_DATETIME,
        'updated_by' => Schema::TYPE_INTEGER,
      ]);
      $this->addPrimaryKey('companies_pk', 'companies', 'id');
      
    }

    public function down()
    {
      $this->dropTable('{{%companies}}');
      $this->dropTable('{{%countries}}');
      $this->dropTable('{{%company_categories}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
