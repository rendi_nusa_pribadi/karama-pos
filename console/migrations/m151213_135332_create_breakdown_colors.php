<?php

use yii\db\Schema;
use yii\db\Migration;

class m151213_135332_create_breakdown_colors extends Migration
{
    public function up()
    {
      $this->createTable('{{%breakdown_colors}}', [
        'id' => 'uuid default uuid_generate_v4()',
        'breakdown_id' => 'uuid',
        'color_name' => Schema::TYPE_STRING,
        
        'created_at' => Schema::TYPE_DATETIME,
        'created_by' => Schema::TYPE_INTEGER,
        'updated_at' => Schema::TYPE_DATETIME,
        'updated_by' => Schema::TYPE_INTEGER,
      ]);
      $this->addPrimaryKey('breakdown_color_pk', 'breakdown_colors', 'id');
    }

    public function down()
    {
      $this->dropTable('{{%breakdown_colors}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
