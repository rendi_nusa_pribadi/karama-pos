<?php

use yii\db\Schema;
use yii\db\Migration;

class m151206_063657_table_breakdowns extends Migration
{
    public function up()
    {
      $this->createTable('{{%breakdowns}}', [
        'id' => 'uuid default uuid_generate_v4()',
        'style' => Schema::TYPE_STRING,
        'body' => Schema::TYPE_STRING,
        'drawsing' => Schema::TYPE_STRING,
        'purchase_order_number' => Schema::TYPE_STRING,
        'description' => Schema::TYPE_TEXT,
        'delivery_date' => Schema::TYPE_DATE,
        'start_period_date' => Schema::TYPE_DATE,
        'end_period_date' => Schema::TYPE_DATE,
        
        'created_at' => Schema::TYPE_DATETIME,
        'created_by' => Schema::TYPE_INTEGER,
        'updated_at' => Schema::TYPE_DATETIME,
        'updated_by' => Schema::TYPE_INTEGER,
      ]);
      $this->addPrimaryKey('breakdown_pk', 'breakdowns', 'id');
    }

    public function down()
    {
      $this->dropTable('{{%breakdowns}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
