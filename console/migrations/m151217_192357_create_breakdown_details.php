<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_192357_create_breakdown_details extends Migration
{
    public function up()
    {
      $this->createTable('{{%breakdown_details}}', [
        'id' => 'uuid default uuid_generate_v4()',
        'breakdown_color_id' => 'uuid',
        'breakdown_id' => 'uuid',
        'hangtag' => Schema::TYPE_STRING,
        'unit_quantity' => Schema::TYPE_INTEGER,
        'ppk_code' => Schema::TYPE_STRING,
        'ppk_quantity' => Schema::TYPE_INTEGER,
        'allowance' => Schema::TYPE_FLOAT,
        
        'created_at' => Schema::TYPE_DATETIME,
        'created_by' => Schema::TYPE_INTEGER,
        'updated_at' => Schema::TYPE_DATETIME,
        'updated_by' => Schema::TYPE_INTEGER,
      ]);
      
      $this->addPrimaryKey('breakdown_detail_pk', 'breakdown_details', 'id');
    }

    public function down()
    {
      $this->dropTable('{{%breakdown_details}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
