<?php

use yii\db\Schema;
use yii\db\Migration;

class m151024_121234_create_user_profiles extends Migration
{
    public function up()
    {
      $this->createTable('{{%user_profiles}}', [
          'id' => 'uuid default uuid_generate_v4()',
          'user_id' => Schema::TYPE_INTEGER,
          'first_name' => Schema::TYPE_STRING,
          'last_name' => Schema::TYPE_STRING,
          'employe_number' => Schema::TYPE_STRING,
          'status' =>  Schema::TYPE_STRING,
          'gender' => "varchar(1)",
          'avatar' => Schema::TYPE_STRING,

          'created_at' => 'datetime default now()',
          'updated_at' => 'datetime default now()',
      ]);
      
      $this->addPrimaryKey('user_profile_pk', 'user_profiles', 'id');
      $this->addForeignKey('profile_user_fk', 'user_profiles', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
      $this->dropTable('{{%user_profiles}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
