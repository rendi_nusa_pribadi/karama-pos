<?php

use yii\db\Schema;
use yii\db\Migration;

class m151107_033552_create_table_quotations extends Migration
{
    public function up()
    {
      $this->createTable('{{%quotations}}', [
        'id' => 'uuid default uuid_generate_v4()',
        'number' => Schema::TYPE_STRING,
        'state' => Schema::TYPE_STRING,
        'date' => Schema::TYPE_DATE,
      ]);
      $this->addPrimaryKey('quotation_pk', 'quotations', 'id');
    }

    public function down()
    {
      $this->dropTable('{{%quotations}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
