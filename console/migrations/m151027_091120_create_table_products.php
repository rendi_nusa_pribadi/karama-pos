<?php

use yii\db\Schema;
use yii\db\Migration;

class m151027_091120_create_table_products extends Migration
{
    public function up()
    {
      $this->createTable('{{%product_types}}', [
        'id' => 'uuid default uuid_generate_v4()',
        'name' => Schema::TYPE_STRING,
      ]);
      
      $this->addPrimaryKey('product_type_pk', 'product_types', 'id');
      
      $this->createTable('{{%products}}', [
          'id' => 'uuid default uuid_generate_v4()',
          'product_type_id' => 'uuid',
          'name' => Schema::TYPE_STRING,
          'picture' => Schema::TYPE_STRING,
          'sold_status' => Schema::TYPE_BOOLEAN,

          'created_at' => 'datetime default now()',
          'created_by_id' => Schema::TYPE_INTEGER,
          'updated_at' => 'datetime default now()',
          'updated_by_id' => Schema::TYPE_INTEGER,
      ]);
      
      $this->addPrimaryKey('product_pk', 'products', 'id');
      $this->addForeignKey('profile_user_fk', 'products', 'product_type_id', 'product_types', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
      $this->dropTable('{{%products}}');
      $this->dropTable('{{%product_types}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
