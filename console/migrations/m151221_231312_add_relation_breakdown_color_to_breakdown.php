<?php

use yii\db\Schema;
use yii\db\Migration;

class m151221_231312_add_relation_breakdown_color_to_breakdown extends Migration
{
    public function up()
    {
      $this->addForeignKey('breakdown_color_breakdwon_fk', 'breakdown_colors', 'breakdown_id', 'breakdowns', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
      
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
