<?php

use yii\db\Schema;
use yii\db\Migration;

class m151024_041306_create_table_notifications extends Migration
{
    public function up()
    {
      //$sql = 'CREATE EXTENSION "uuid-ossp";';
      //$this->execute($sql);
      $this->createTable('{{%setup_notifications}}', [
          'id' => 'uuid default uuid_generate_v4()',
          'user_id' => Schema::TYPE_INTEGER,
          'url' => Schema::TYPE_STRING,
          'notification' => Schema::TYPE_STRING,
          'status' =>  Schema::TYPE_STRING,

          'created_at' => 'datetime default now()',
      ]);
      
      $this->addPrimaryKey('setup_notification_pk', 'setup_notifications', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%setup_notifications}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
