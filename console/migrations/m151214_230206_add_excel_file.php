<?php

use yii\db\Schema;
use yii\db\Migration;

class m151214_230206_add_excel_file extends Migration
{
    public function up()
    {
      $this->addColumn('breakdown_colors', 'excel_file', Schema::TYPE_STRING); 
    }

    public function down()
    {
      $this->dropColumn('breakdown_colors', 'excel_file');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
