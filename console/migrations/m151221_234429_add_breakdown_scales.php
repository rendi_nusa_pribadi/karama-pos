<?php

use yii\db\Schema;
use yii\db\Migration;

class m151221_234429_add_breakdown_scales extends Migration
{
    public function up()
    {
      $this->createTable('{{%breakdown_scales}}', [
        'id' => 'uuid default uuid_generate_v4()',
        'breakdown_color_id' => 'uuid',
        'size' => Schema::TYPE_STRING,
        
        'created_at' => Schema::TYPE_DATETIME,
        'created_by' => Schema::TYPE_INTEGER,
        'updated_at' => Schema::TYPE_DATETIME,
        'updated_by' => Schema::TYPE_INTEGER,
      ]);
      $this->addPrimaryKey('breakdown_scales_pk', 'breakdown_scales', 'id');
      
      $this->createTable('{{%breakdown_ppk_scales}}', [
        'id' => 'uuid default uuid_generate_v4()',
        'breakdown_scale_id' => 'uuid',
        'ppk' => Schema::TYPE_STRING,
        'scale' => Schema::TYPE_INTEGER,
        
        'created_at' => Schema::TYPE_DATETIME,
        'created_by' => Schema::TYPE_INTEGER,
        'updated_at' => Schema::TYPE_DATETIME,
        'updated_by' => Schema::TYPE_INTEGER,
      ]);
      
      $this->addPrimaryKey('breakdown_ppk_scales_pk', 'breakdown_ppk_scales', 'id');
    }

    public function down()
    {
      $this->dropTable('{{%breakdown_scales}}');
      $this->dropTable('{{%breakdown_ppk_scales}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
